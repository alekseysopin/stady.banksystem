﻿using System;
using System.Collections.Generic;

namespace BankSystem
{
  class Program
  {
    public static bool IsMenuAdmin = true;
    public static int read = default;
    public static int userIndex;

    static void Main(string[] args)
    {
      while (true)
      {
        var addUser = new UserService(); 
        var user = new UserModel
        {
          Name = "Vasia",
          Login = "test",
          Password = "123",
          Amount = 100,
          Role = Role.User
        };
        var user1 = new UserModel
        {
          Name = "Kolia",
          Login = "testk",
          Password = "321",
          Amount = 25,
          Role = Role.User
        };
        var user2 = new UserModel
        {
          Name = "Lena",
          Login = "testl",
          Password = "1234",
          Amount = 56,
          Role = Role.User
        };
        var user3 = new UserModel
        {
          Name = "Admin",
          Login = "admin",
          Password = "admin",
          Amount = 0,
          Role = Role.Admin
        };
        var users = new List<UserModel>()
        {
          user,
          user1,
          user2,
          user3
        };

        Console.WriteLine("Enter login and password");
        var login = Console.ReadLine();
        var pass = Console.ReadLine();
        Console.Clear();

        userIndex = LoginPass.LoginPassword(users, login, pass,userIndex);
        /*User Menu*/
        if (users[userIndex].Login == login && users[userIndex].Password == pass && users[userIndex].Role == Role.User)
        {
          var menuPosition = 0;
          var userMenu = new List<string>()
          {
            "See me deposit",
            "Put money into your account",
            "Withdraw money from the account",
            "Exit"
          };
          while (true)
          {
            Menu(userMenu, menuPosition);

            ConsoleKey usermenuKey = Console.ReadKey().Key;
            if (usermenuKey == ConsoleKey.DownArrow)
            {
              menuPosition++;
            }
            if (menuPosition == userMenu.Count)
            {
              menuPosition = 0;
            }
            if (usermenuKey == ConsoleKey.UpArrow)
            {
              menuPosition--;
            }
            Console.Clear();
            if (menuPosition == -1)
            {
              menuPosition = 3;
            }
            if (usermenuKey == ConsoleKey.Enter && menuPosition == 0)
            {
              UserAmount(login, userIndex);
            }
            if (usermenuKey == ConsoleKey.Enter && menuPosition == 1)
            {
              AddToAccount(users[userIndex], read);
            }
            if (usermenuKey == ConsoleKey.Enter && menuPosition == 2)
            {
              Console.WriteLine("How much do you want to withdraw from your account?: " + " ");
              users[userIndex].Amount = users[userIndex].Amount - Convert.ToInt32(Console.ReadLine());
              Console.Clear();
            }
            if (usermenuKey == ConsoleKey.Enter && menuPosition == 3)
            {
              break;
            }
            if (usermenuKey == ConsoleKey.Escape)
            {
              break;
            }
          }
        }
        /*админ меню*/
        if (users[userIndex].Login == login && users[userIndex].Password == pass && users[userIndex].Role == Role.Admin)
        {
          var menuPosition = 0;
          var adminMenu = new List<string>()
          {
            "See user list",
            "Add user",
            "Delete user",
            "Change username",
            "Exit to login"
          };
          while (true)
          {
            Menu(adminMenu, menuPosition);

            ConsoleKey consoleKey = Console.ReadKey().Key;
            if (consoleKey == ConsoleKey.DownArrow)
            {
              menuPosition++;
            }
            if (menuPosition == adminMenu.Count)
            {
              menuPosition = 0;
            }
            if (consoleKey == ConsoleKey.UpArrow)
            {
              menuPosition--;
            }
            Console.Clear();
            if (menuPosition == -1)
            {
              menuPosition = 4;
            }
            if (consoleKey == ConsoleKey.Enter && menuPosition == 0)
            {
              for (var i = 0; i < users.Count; i++)
              {
                Console.WriteLine(users[i].Name);
              }
            }
            if (consoleKey == ConsoleKey.Enter && menuPosition == 1)
            {
              users = addUser.AddUser(users);
            }
            if (consoleKey == ConsoleKey.Enter && menuPosition == 2)
            {
              users = UserService.RemoveUser(users, menuPosition);
            }
            if (consoleKey == ConsoleKey.Enter && menuPosition == 3)
            {
              users = UserService.ChangeNameUser(users, menuPosition);
            }
            if (consoleKey == ConsoleKey.Enter && menuPosition == 4)
            {
              IsMenuAdmin = false;
              break;
            }
          }
        }
        void Menu(List<string> userMenu, int menuPosition)
        {
          for (int i = 0; i < userMenu.Count; i++)
          {
            if (i == menuPosition)
            {
              Console.BackgroundColor = ConsoleColor.DarkGray;
              Console.WriteLine(userMenu[i]);
              Console.ResetColor();
              continue;
            }
            Console.WriteLine(userMenu[i]);
          }
          return;
        }
        void UserAmount(string login, int userIndex)
        {
          if (login == users[userIndex].Login)
          {
            Console.WriteLine(users[userIndex].Amount);
          }
        }
        void AddToAccount(UserModel user, int read)
        {
          Console.WriteLine("How much do you want to top up your account?: " + " ");
          read = Convert.ToInt32(Console.ReadLine()) + user.Amount;
          users[userIndex].Amount = read;
          Console.Clear();
        }
      }
    }
  }
}
