﻿using System;
using BankSystem;
using System.Collections.Generic;

namespace BankSystem
{
  class UserService
  {
    private int z = 4;

    public List<UserModel> AddUser(List<UserModel> users)
    {
      users.Add(new UserModel());
      {
        Console.WriteLine("Enter the name of the new user: " + " ");
        users[z + 1].Name = Console.ReadLine();
        Console.WriteLine("Enter the Login of the new user: " + " ");
        users[z + 1].Login = Console.ReadLine();
        Console.WriteLine("Enter the Password of the new user: " + " ");
        users[z + 1].Password = Console.ReadLine();
        users[z + 1].Role = Role.User;
      }
      return users;
    }
        public static List<UserModel> RemoveUser(List<UserModel> users, int menuPosition)
    {
      while (true)
      {
        for (int i = 0; i < users.Count; i++)
        {
          if (i == menuPosition)
          {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.WriteLine(users[i].Name);
            Console.ResetColor();
            continue;
          }
          Console.WriteLine(users[i].Name);
        }
        ConsoleKey usermenuKey = Console.ReadKey().Key;
        if (usermenuKey == ConsoleKey.DownArrow)
        {
          menuPosition++;
        }
        if (menuPosition == users.Count)
        {
          menuPosition = 0;
        }
        if (usermenuKey == ConsoleKey.UpArrow)
        {
          menuPosition--;
        }
        Console.Clear();
        if (menuPosition == -1)
        {
          menuPosition = users.Count - 1;
        }
        if (usermenuKey == ConsoleKey.Escape)
        {
          break;
        }
        if (usermenuKey == ConsoleKey.Enter)
        {
        users.Remove(users[menuPosition]);
          break;
        }
      }
      return users;
    }

    public static List<UserModel> ChangeNameUser(List<UserModel> users, int menuPosition)
    {
      while (true)
      {
        for (int i = 0; i < users.Count; i++)
        {
          if (i == menuPosition)
          {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.WriteLine(users[i].Name);
            Console.ResetColor();
            continue;
          }
          Console.WriteLine(users[i].Name);
        }
        ConsoleKey usermenuKey = Console.ReadKey().Key;
        if (usermenuKey == ConsoleKey.DownArrow)
        {
          menuPosition++;
        }
        if (menuPosition == users.Count)
        {
          menuPosition = 0;
        }
        if (usermenuKey == ConsoleKey.UpArrow)
        {
          menuPosition--;
        }
        Console.Clear();
        if (menuPosition == -1)
        {
          menuPosition = users.Count - 1;
        }
        if (usermenuKey == ConsoleKey.Escape)
        {
          break;
        }
        if (usermenuKey == ConsoleKey.Enter && menuPosition != -1)
        {
          Console.Clear();
          var changeuser = Console.ReadLine();
          for (var i = 0; i <= 1; i++)
          {
            if (changeuser != users[i].Name)
            {
              users[menuPosition].Name = changeuser;
            }
            else
            {
              continue;
            }
          }
          if (usermenuKey == ConsoleKey.Enter)
          {
            break;
          }
        }
      }
      return users;
    }
  }
}



