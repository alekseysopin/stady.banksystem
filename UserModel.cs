﻿using System;
using BankSystem;
using System.Collections.Generic;

namespace BankSystem
{
  public class UserModel
  {
    public string Name;
    public string Login;
    public string Password;
    public Role Role;
    public int Amount;
  }
  public enum Role
  {
    None = 0,
    Admin = 1,
    User = 2
  }
}
