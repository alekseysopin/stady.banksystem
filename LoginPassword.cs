﻿using System;
using BankSystem;
using System.Collections.Generic;

namespace BankSystem
{
  public class LoginPass
  {
    public static int LoginPassword(List<UserModel> users, string login, string pass, int userIndex)
     {
      for (var i = 0; i < users.Count; i++)
      {
        userIndex = i;
        if (login == users[i].Login && pass == users[i].Password)
        {
          Console.BackgroundColor = ConsoleColor.Green;
          Console.WriteLine("welcome to Bank  :" + users[userIndex].Name);
          Console.ResetColor();
          break;
        }
        if (login == users[userIndex].Login && pass == users[userIndex].Password && users[userIndex].Role == Role.Admin)
        {
          Console.BackgroundColor = ConsoleColor.DarkYellow;
          Console.WriteLine("welcome to Bank  :" + users[userIndex].Name);
          Console.ResetColor();
          break;
        }
      }
      return userIndex;
    }
  }
}